const { PubSub } = require('@google-cloud/pubsub')
const { serialize } = require('v8')
const project = require('./project.json')
const data = require('./pub.json')

async function run() {
  const pubsub = new PubSub({
    apiEndpoint: 'http://localhost:8085',
    projectId: project.projectId
  })

  try {
    const topic = pubsub.topic(data.topic)
    const message = JSON.stringify(data.message)
    console.log(`Message: ${message}`)

    const messageId = await topic.publish(Buffer.from(message), data.attributes)

    console.log(`Published: ${messageId}`)
  } catch (error) {
    if (error.code == 5) {
      await pubsub.createTopic(data.topic)

      run()
      return
    }

    console.error(error)
  }
}

run()
